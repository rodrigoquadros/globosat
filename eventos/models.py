from django.db import models
from django.utils import timezone
from datetime import date

class Evento(models.Model):
    evento_calendario = models.DateField(default=date.today)
    evento_titulo = models.CharField(max_length=200, default="Título do evento")
    evento_subtitulo = models.CharField(max_length=200, default='Subtítulo do evento')
    evento_img_arquivo = models.FileField(upload_to='img/eventos', null=True,)
    def __str__(self):
        return self.evento_titulo

